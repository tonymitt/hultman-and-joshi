import React from 'react'
import Layout from '../components/Layout'
import Hero from '../components/Hero'

export const AreasOfPractice = () => {

  return (
    <Layout>
        <section className="section section--gradient">
        <div className="container">
            <div className="row">
            <div className="col-md-2">
            </div>
            <div className="col-md-8">
                <Hero hero={'img/about-hero.jpg'} />
                <div className="row" style={{marginTop: '24px'}}>
                <div className="col-md-4">
                    <img src="img/lori.jpg" style={{width: '100%'}}/>
                </div>
                <div className="col-md-8">
                    <h1 style={{marginTop: 0}}>Lori Hultman</h1>
                    <p>
                    Lori Hultman is a Florida native with a BA and law degree from the University of Florida. She is Florida Bar Board Certified in Labor & Employment Law and has about 37 years experience representing employers, both in house and in private law practice. While in-house counsel for large financial institutions, she managed a department of attorneys, paralegals and support staff. This first hand management experience, combined with defending and advising a wide variety of businesses enables her to evaluate issues and recommend solutions to employment challenges from a practical, business standpoint, with an appreciation for her client’s budgetary concerns.
                    </p>
                    
                </div>
                
                </div>
                <div style={{
                        padding: '15px 24px',
                        color: '#FFF',
                        fontSize: '16px',
                        backgroundColor: '#81397F',
                        margin: '24px 0px'
                    }}>
                        Florida Bar, Board Certified in Labor and Employment Law
                    </div>
                    <p>
                    My law career started at a small savings & loan in Miami. Six years later I was General Counsel but wanted to try something a little different. I took and passed the California Bar and my son and I moved to Los Angeles. There I worked for Great Western Bank, which is now JP Morgan Chase. My primary duties were providing advice and counsel and managing litigation for all employment matters and providing legal advice for the banking products of this multistate financial institution.
                    </p>
                    <p>
                    Growing up in Florida, I knew hurricanes, but unlike earthquakes, they are somewhat predictable (and have no aftershocks). Following the serious Northridge earthquake, I returned to Florida and practiced on my own for a while. Then, I took a job in a firm that practiced only employment law. During my sixteen years there, I did litigation defense handling discrimination claims, contract issues, non-compete etc., EEOC and other administrative charges, along with counseling and providing preventive advice. My depth of employment law experience allowed me to be in the first group of attorneys to earn the Florida Bar Certification in Labor and Employment.        
                    </p>
                    <p>
                    As in-house counsel, I not only handled and advised managers about HR matters, but I was responsible for managing my department of attorneys, paralegals and support staff. In this management role I gained first hand knowledge about the importance of evaluating and resolving employment matters from a practical, business standpoint, while keeping in mind budgetary constraints. This experience has served me well in representing employers over the past decades.
                    </p>
                    <p>
                    I am a member of Sarasota County Bar Member and formerly Co-Chair of Labor and Employment Section. I have served on the Board of Sarasota Florida Association of Women Lawyers and was a member of the Academy of Florida Management Attorneys. I belong to the Labor and Employment Law and Trial Lawyers Sections of the Florida Bar. My honors include being selected as a SuperLawyer in 2008-2010, and from 2014 through 2020.
                    </p>
                    <p>
                    In addition to employment law, my other passions are my husband and family, practicing yoga, glassblowing and creating glass art. 
                    </p>
            </div>
            <div className="col-md-2">
            </div>
            </div>
        </div>
        </section>
    </Layout>
  )
}


export default AreasOfPractice

