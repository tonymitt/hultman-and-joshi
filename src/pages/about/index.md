---
templateKey: 'about-page'
path: /about
title: About Hultman + Joshi
hero: /img/about-hero.jpg
---
Lori Hultman and Nik Joshi formed this labor and employment law firm of Hultman + Joshi to practice law differently, by practicing in a manner that reflects their personalities while building long-term relationships. Both Lori and Nik have been Board Certified as specialists in Labor and Employment Law by the Florida Bar. Lori has consistently been selected as a SuperLawyer by that organization over the last decade. 

Hultman + Joshi provides businesses and organizations (both private and public sector) with expert advice and counsel on workplace and business management issues. Hultman + Joshi also brings 65-plus years of experience in defending both private and public sector employers in courts or in front of governmental agencies. Hultman + Joshi’s services include preventative measures such as advising employers when hiring, disciplining and terminating employees, drafting policies, handbooks, independ ent contractor agreements, non-compete, employment agreements and a variety of other personnel and legal documents. Hultman + Joshi offers workforce training to ensure EEO and HIPAA compliance, conducts workplace investigations, and provides assistance to clients on governmental investigations and audits of various types. Hultman + Joshi also defends organizations and businesses when responding to EEOC/FCHR and Human Rights ordinance charges and claims of discrimination, harassment and retaliation (including whistleblower claims), claims of wage and hour (overtime) violation and the Family and Medical Leave Act claims. Hultman + Joshi, further, has significant experience in managing  union issues, including collective bargaining. 

As Hultman+Joshi partners with its clients to guide them through the maze of laws and regulations that face employers and businesses on a daily basis, that is why it believes, You+Us= Workplace Solutions.