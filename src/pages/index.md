---
templateKey: index-page
hero: /img/home-hero.jpg
aboutSection:
  headline: You + Us = Workplace Solutions
  description: >
    Lori Hultman and Nikhil Joshi are Florida-Bar Certified as Specialists in Labor & Employment Law. Only approximately one (1) percent of attorneys are Certified as Specialists by the Florida Bar. That along with nearly 65-combined years of experience as attorneys, if you are a business owner or management official needing representation for your business’ legal needs, rest assured that Hultman + Joshi will smartly advocate on your behalf to protect your interests, and will explore practical, experienced-based solutions that address proactively your needs and objectives. Regardless of the legal dispute or business challenge facing you, Lori and Nik will provide experienced, pragmatic, solutions-based counsel, advocacy and representation to protect you and your bottom-line.
areasOfPractice:
  headline: Areas of Practice
  subheadline: Areas of Practice
  area:
    - title: Employee Relations Counseling
      description: >
        We advise clients on compliance with employment-related statutes and
        regulations on the federal, state and local levels. We take a
        preventative approach in counseling clients, reflecting our client's
        goal to minimize conflicts in the workplace that may lead to costly
        litigation.
      url: '/areas-of-practice#'
    - title: Employment Litigation
      description: >-
        I represent clients at trial in state and federal courts and in
        appellate proceedings. We also handle arbitrations and administrative
        hearings before state and federal anti-discrimination agencies or labor
        relations boards, as well as before unemployment compensation
        tribunals. 
      url: '/areas-of-practice#'
    - title: Workplace Investigations
      description: >-
        I conduct and counsel employers on workplace investigations, including
        allegations of employee misconduct. I am frequently called upon to
        assist with investigations like, employment discrimination allegations
        and compliance with corporate ethics policies.
    - title: Employee Handbooks
      description: >-
        Employee handbooks protect you from such issues as discrimination
        charges. Employees feel that they are treated equally when the procedure
        is written in the handbook, and the procedure is followed and fairly
        applied to all employees.
    - title: Special Employment Services
      description: >-
        I also provide specialized employment-related services to address the
        business needs of our clients. These services include, employment
        practice audits, seminars and workshops on significant developments and
        customized training programs.
    - title: Employment Training & Workshops
      description: >-
        I've developed and conducted training programs for both supervisory and
        non-supervisory employees. The training programs and workshops are
        designed to meet a client's specific needs in a wide variety of
        workplace issues.
testimonials:
  testimonial:
    - quote: >-
        “Over the Years the Hultman + Joshi team has provided Absolute Aluminum with sound, clear, and practical legal advice on various issues. They are diligent  at setting companies and individuals up for success. There is nothing better than realizing right when an issue comes up that you are covered thanks to their pre-planning of your possible liabilities. The most welcoming quality is they are a down to earth group that doesn’t just tell you what you want to hear.” 
      name: Adam Rankin - Vice President - Absolute Aluminum, Inc., Venice/Sarasota
    - quote: >-
        “Trust is the foundation of relationships and Hultman + Joshi are our trusted legal advisors, with a personal touch – we feel so much more than ‘a client’ – we feel their care and dedication to our business and to us as people.”
      name: Debbie Muno, President - Genos North America, Tampa, Florida 
    - quote: >-
        “We have used Hultman & Joshi in a variety of ways in the past three years.  They are compassionate, concerned, knowledgeable, and timely. We give them our highest recommendation. ”
      name: Murray Chase, Executive Director -  Venice Theatre, Venice, Florida 
    - quote: >-
        “For more than 10 years working with Nik, he is always there for me during the most difficult time or the worst cases. Not only the professionalism but also the care Nik demonstrates to me handling all those hardships is the reason I always think about him and only him whenever needed. Where else can you find such a labor attorney to be on you (the owner’s) side?”
      name: Jonathan S.Y. Cheng - Vice President - Merits Health Products CO., LTD, Taiwan and Cape Coral, Florida 
    - quote: >-
        “I am grateful for the help that Nik provided me during the very difficult times of COVID 19 related employee support. I had some complex employment issues/ concerns that he helped me with the larger picture in mind. He understands how to protect your business and helps you plan day to day employment operations to avoid future complex problems.  His prompt response was extremely helpful and avoided lots of stressful situations.  He was amazing, and he treated me as if I was one of their most important clients. I have never worked with another professional as responsive as Nick.  For that, Nikhil (Nik) gets my respect and my unqualified endorsement.  He is one phone call/text away”   
      name: Mr. Shashi Galipalli, Forever Veterinarians, Jacksonville, Florida  
    - quote: >-
        “We have worked with the attorneys and support team at Hultman Joshi since 2018 on matters involving the EEOC and the NLRB and have been pleased with not only the outcome of the matters but the relationship developed over the years.  While the firm is outside counsel, they have learned the nuances of our business as if they were members of our organization which enables us to work more efficiently and reach outcomes that meet our business goals.”
      name: Angela Ward, Corporate General Counsel, MasterCorp, Nashville, Tennessee
    - quote: >-
        “Hultman + Joshi PA is the absolute best! Nik cares about the company in the big picture, not just helping on a particular issue. Anytime you need anything they are there in a timely fashion with great advice, and wonderful communication. We would highly recommend Nik and Hultman+Joshi!!”
      name: Jon and  Danielle Krawczyk, Superior Pools, Port Charlotte, Florida 
    - quote: >-
        “Nikhil and his firm really look out for the hotel community’s best interests when faced with legal risks and challenges. I’ve always found him to be relatable, easy to speak with, and trustworthy. A true counselor. Not just a lawyer.”
      name: Bharat Patel, owner of hotels/businesses, and national AAHOA leader, Sarasota, Florida


---
