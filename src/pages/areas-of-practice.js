import React from 'react'
import Layout from '../components/Layout'
import Hero from '../components/Hero'

export const AreasOfPractice = () => {

  return (
    <Layout>
        <section className="section section--gradient">
        <div className="container">
            <div className="row">
            <div className="col-md-2">
            </div>
            <div className="col-md-8">
                <div className="section">
                    <Hero hero={'img/about-hero.jpg'} />
                    <h2 className="title is-size-3 has-text-weight-bold is-bold-light">
                        Areas of Practice
                    </h2>
                    <strong>Employee Relations Counseling and Risk Avoidance Management </strong>
                    <p>
                    H+J attorneys advise clients on compliance with employment-related statutes and regulations on the federal, state and local levels. H+J attorneys take a preventative approach in counseling clients, reflecting our client's goal to minimize conflicts in the business and workplace that may lead to costly litigation. H+J attorneys regularly assist clients in the following areas:
                    </p>
                    <p>
                    Advice and Counsel on HR and EEO Compliance<br />
                    Interview and Hiring Tips<br />
                    Employee Counseling<br />
                    Employee Performance Improvement Plan<br />
                    Performance Evaluation Forms and Tools<br />
                    Effective Disciplinary Documentation<br />
                    Employee Discipline and Termination <br />
                    Just Cause Discipline<br />
                    Fair Credit Reporting Act Compliance<br />
                    Fair Labor Standards Act Compliance<br />
                    Family and Medical Leave Act Compliance<br />
                    Leave Management (FMLA, pregnancy, disability, sick leave, worker’s compensation)<br />
                    OSHA (workplace safety) Compliance<br />
                    Workplace Safety Audit and Compliance<br />
                    Immigration (I-9) Compliance<br />
                    Overtime Liability Avoidance Strategies<br />
                    Reductions in Force Planning<br />
                    COVID-19/Pandemic Protocols<br />
                    HIPAA Protocols<br />
                    Data Breach<br />
                    Disability/Reasonable Accommodation Planning<br />
                    Discrimination Avoidance <br />
                    Harassment Avoidance<br />
                    Retaliation Avoidance<br />
                    Workplace Bullying Avoidance<br />
                    Drug-Free Workplace Policies<br />
                    Medical Marijuana in the Workplace <br />
                    EEO Supervisory and General Workforce Training <br />
                    Sexual and Other Forms of Harassment Training<br />
                    Hostile Working Environment Training<br />
                    HIPAA Training and Compliance<br />
                    Union Avoidance Training<br />
                    Worker’s Compensation Retaliation Avoidance <br />
                    </p>
                    <p>
                        <strong>Business Management Counseling, Coaching and Legal Documents</strong>
                    </p>
                    <p>
                    H+J attorneys work with high-level executives, management officials and supervisors on critical business and personnel-planning issues. These include but are not limited to: 
                    </p>
                    <p>
                    Management and Executive EEO Compliance Training <br />
                    Business Crisis Handling and Counseling (positive public relations) <br />
                    Leadership/Management Transition Planning (hiring, promotions, retirements and separations)<br />
                    Business Formation Legal Agreements<br />
                    Employment Agreements (C-Suite and Others)<br />
                    Operating Agreements<br />
                    Partnership and Membership Agreements<br />
                    Corporation-related documentation<br />
                    Trade Secrets Protection Planning<br />
                    Intellectual Property Protection Planning<br />
                    Social Media-related Risk Management <br />
                    Promissory Note and Guaranty Documentation <br />
                    </p>
                    <p>
                        <strong>
                            Personnel Documents and Contracts - Counseling, Drafting and Implementation
                        </strong>
                    </p>
                    <p>
                    H+J attorneys review, revise and draft documents of all types in order to best protect the client. The scope and types of documents include the following: 
                    </p>
                    <p>
                        Employment Agreements <br />
                        Job Descriptions<br />
                        New-Hire Onboarding Documents<br />
                        Employment Applications <br />
                        Non-Competition Agreements<br />
                        Non-Disclosure Agreements<br />
                        Workplace-Related Forms and Policies<br />
                        Employee Handbooks/Manuals<br />
                        Severance Agreements<br />
                        Separation/Termination Agreements<br />
                        Last-Chance Agreements<br />
                        Performance Improvement Plans<br />
                        Disciplinary and Termination Documentation<br />
                        Employee Counseling<br />
                        Performance Evaluation Forms and Tools<br />
                    </p>
                    <p>
                        <strong>Administrative Law Matters (EEOC, NLRB, DOL, PERC, DHS, OSHA, INS)</strong>
                    </p>
                    <p>
                        U.S. Department of Labor Wage and Hour Audits and Investigations<br />
                        U.S. Department of Justice Housing Discrimination Matters<br />
                        U.S. Equal Employment Opportunity Commission Charges of Discrimination (race, religion, national origin, gender/sex, sexual orientation)<br />
                        Florida Commission on Human Relations Charges of Discrimination  (race, religion, national origin, gender/sex, sexual orientation)<br />
                        Florida Commission on Human Relations Housing Discrimination Matters<br />
                        Unfair Labor Practices under U.S. National Labor Relations Board<br />
                        Representation and Collective Bargaining under U.S. National Labor Relations Act<br />
                        Unfair Labor Practices under Florida Public Employees Relations Act<br />
                        Representation and Collective Bargaining under Florida Public Employees Relations Act<br />
                        OSHA Audits and Complaints <br />
                        Unemployment/Reemployment Appeals<br />
                        U.S. Department of Homeland Security - Workplace Immigration Audits and Compliance <br />
                        Grievance Arbitration<br /> 
                    </p>
                    <p>
                        <strong>
                            Federal Court and Florida Court Litigation 
                        </strong>
                    </p>
                    <p>
                        When disputes cannot be avoided, H+J attorneys represent clients at trial in state and federal courts and in appellate proceedings. H+ J attorneys also handle arbitrations and administrative hearings before state and federal anti-discrimination agencies or labor relations boards, as well as before unemployment compensation tribunals. H+J attorneys approach cases aggressively, frequently leading to positive results and favorable settlements early in the litigation process. The firm represents and defends businesses and management officials in state and federal court across Florida in cases alleging violations of Title VII of the Civil Rights Act of 1964 (race, religion, national origin, gender/sex, sexual orientation), Florida Civil Rights Act, Americans with Disabilities Act, Family and Medical Leave Act, Fair Labor Standards Act, Age Discrimination in Employment Act, Pregnancy Discrimination Act, breach of contract, workplace and/or business tort claims, breach of non-competition agreements, defamation, slander, unpaid wages, unpaid overtime, plus other types of matters. 
                    </p>
                    <p>
                        <strong>Workplace Investigations</strong>
                    </p>
                    <p>
                    H+J attorneys are experienced in conducting effective, efficient, fact-based and unbiased investigations into allegations of workplace misconduct or impropriety, with findings and recommendations issued (in writing or verbally as per client preference) at the conclusion of the investigation. H+J attorneys conduct and counsel employers on workplace investigations, including allegations of employee misconduct. H+J is frequently called upon to assist with investigations into:
                    </p>
                    <p>
                    bullying, sexual, racial, disability, ethnic and religious harassment<br />
                    possession, use and/or sale of controlled substances<br />
                    compliance with corporate ethics policies<br />
                    workplace violence<br />
                    employment discrimination allegations<br />
                    employee theft<br />
                    non-compliance with workplace policies<br />
                    </p>
                    <p>
                        <strong>Training and Workshops</strong>
                    </p>
                    <p>
                    H+J works together with clients to develop, implement or conduct training programs for both supervisory and non-supervisory employees. The training programs and workshops are designed to meet a client's specific needs in a wide variety of workplace issues, such as:
                    </p>
                    <p>
                    sexual or discriminatory harassment<br />
                    prevention of workplace violence<br />
                    compliance with employment discrimination laws<br />
                    proper hiring practices<br />
                    minimizing liability in discipline and discharge decisions<br />
                    compliance with health and safety obligations<br />
                    federal and state wage and hour compliance<br />
                    </p>
                </div>
            </div>
            <div className="col-md-2">
            </div>
            </div>
        </div>
        </section>
    </Layout>
  )
}


export default AreasOfPractice

