import React from 'react'
import Layout from '../../components/Layout'
import BlogRoll from '../../components/BlogRoll'

export default class NewsIndexPage extends React.Component {
  render() {
    return (
      <Layout>
        <div className="off-white-section">
          <h2 className="section-title" style={{ marginBottom: '8px' }}>Our Latest</h2>
          <h3 className="center">News &amp; Updates</h3>
          <BlogRoll />
        </div>
      </Layout>
    )
  }
}
