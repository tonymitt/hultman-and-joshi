import React from 'react'
import { navigate } from 'gatsby-link'
import Layout from '../../components/Layout'

function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
}

export default class Index extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isValidated: false }
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()
    const form = e.target
    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({
        'form-name': form.getAttribute('name'),
        ...this.state,
      }),
    })
      .then(() => navigate(form.getAttribute('action')))
      .catch(error => alert(error))
  }

  render() {
    return (
      <Layout>
        <section className="section">
          <div className="container">
            <div className="row">
              <div className="col-md-2">
              </div>
              <div className="col-md-8">
              </div>
              <div className="col-md-2">
              </div>
            </div>
            <div className="row">
              <div className="col-md-2">
              </div>
              <div className="col-md-8">
                <h1 className="section-title" style={{ textAlign: 'left' }}>Contact Us</h1>
              </div>
              <div className="col-md-2">
              </div>
            </div>
            <div className="row">
              <div className="col-md-2">
              </div>
              <div className="col-md-3">
                <h4>Address</h4>
                <p>Hultman + Joshi <br />
                  2055 Wood St. <br />
                  Ste 208 <br />
                  Sarasota, FL 34237
                </p>
              </div>
              <div className="col-md-3 contact-page-contact">
                <h4>Contact</h4>
                <p>
                  <a href="mailto:info@hultmanjoshi.com" className="contact-element"><span>info@hultmanjoshi.com</span></a><br />
                  <span className="contact-element"><span>941.218.2800 </span></span><br />
                  <span className="contact-element"><span>941.218.2801</span></span>
                </p>
              </div>
              <div className="col-md-2">
              </div>
            </div>
            <div className="row">
              <div className="col-md-2">
              </div>
              <div className="col-md-8">

                <form
                  name="contact"
                  method="post"
                  action="/contact/thanks/"
                  data-netlify="true"
                  data-netlify-honeypot="bot-field"
                  onSubmit={this.handleSubmit}
                >
                  {/* The `form-name` hidden field is required to support form submissions without JavaScript */}
                  <input type="hidden" name="form-name" value="contact" />
                  <div hidden>
                    <label>
                      Don’t fill this out:{' '}
                      <input name="bot-field" onChange={this.handleChange} />
                    </label>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="field">
                        <label className="label" htmlFor={'name'}>
                          First Name*
                      </label>
                        <div className="control">
                          <input
                            className="input"
                            type={'text'}
                            name={'firstname'}
                            placeholder="First Name"
                            onChange={this.handleChange}
                            id={'name'}
                            required={true}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="field">
                        <label className="label" htmlFor={'name'}>
                          Last Name*
                      </label>
                        <div className="control">
                          <input
                            className="input"
                            type={'text'}
                            name={'lastname'}
                            placeholder="Last Name"
                            onChange={this.handleChange}
                            id={'name'}
                            required={true}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="field">
                        <label className="label" htmlFor={'name'}>
                          Phone
                      </label>
                        <div className="control">
                          <input
                            className="input"
                            type={'text'}
                            name={'lastname'}
                            placeholder="Phone Number"
                            onChange={this.handleChange}
                            id={'name'}
                            required={false}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="field">
                        <label className="label" htmlFor={'email'}>
                          Email
                        </label>
                        <div className="control">
                          <input
                            className="input"
                            type={'email'}
                            name={'emailaddress'}
                            onChange={this.handleChange}
                            id={'email'}
                            required={true}
                            placeholder="Email Address*"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="field">
                        <label className="label" htmlFor={'message'}>
                          Message
                        </label>
                        <div className="control">
                          <textarea
                            className="textarea"
                            name={'message'}
                            onChange={this.handleChange}
                            id={'message'}
                            placeholder="How Can We Help?*"
                            required={true}
                          />
                        </div>
                      </div>
                      <p style={{ fontSize: '14px', color: '#58595B', marginTop: '0px' }}>
                        *Please do not include confidential or sensitive information in your message. In the event that we are representing a party with opposing interests to your own, we may have a duty to disclose any information you provide to our client.
                      </p>
                    </div>
                  </div>
                  <div className="field">
                    <button className="button is-link" type="submit">
                      Send
                  </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </Layout >
    )
  }
}
