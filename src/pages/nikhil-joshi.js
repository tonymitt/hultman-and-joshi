import React from 'react'
import Layout from '../components/Layout'
import Hero from '../components/Hero'

export const AreasOfPractice = () => {

  return (
    <Layout>
        <section className="section section--gradient">
        <div className="container">
            <div className="row">
            <div className="col-md-2">
            </div>
            <div className="col-md-8">
                <Hero hero={'img/about-hero.jpg'} />
                <div className="row" style={{marginTop: '24px'}}>
                <div className="col-md-4">
                    <img src="img/nik.jpg" style={{width: '100%'}}/>
                </div>
                <div className="col-md-8">
                    <h1 style={{marginTop: 0}}>Nikhil N. Joshi</h1>
                    <p>
                    I am Mr. Nikhil (Nik) Joshi, Esq. I am Florida Bar, Board Certified as a specialist in Labor and Employment Law. Practicing since 1997, I have over two decades of experience representing employers and business owners, and take pride in providing thoughtful, pragmatic advice and counsel to my clients. A native Floridian and proud alum of the Gator Nation, I also defend business owners and employers of all sizes in federal and state court litigation, arbitrations and administrative matters, both inside and outside the state of Florida, and strive to do so in a cost-effective manner that meets the client’s best financial and strategic interests. I provide training to management officials and employees on various topics and areas of the law. I also represent private and public sector employers in collective bargaining and Union-related matters.
                    </p>
                    
                </div>
                
                </div>
                <div style={{
                        padding: '15px 24px',
                        color: '#000',
                        fontSize: '16px',
                        backgroundColor: '#8CC63E',
                        margin: '24px 0px'
                    }}>
                        Florida Bar, Board Certified in Labor and Employment Law
                    </div>
                    <p>
                    Since 1997, I have lectured and presented to various professional, civic and charitable organizations on matters and topics involving labor and employment law. My presentations have taken place locally in the Sarasota-Bradenton-Tampa-Fort Myers areas, as well as across the United States at national conferences and seminars. I have also authored several legal summaries for seminar materials that have been presented both statewide and nationally, have served as the Co-Chair of the Sarasota County Bar Association Labor and Employment Law Section, and remain active in many different community groups and charitable organizations. I am also an Alliance Member of the Asian-American Hotel Owners Association. 
                    </p>
                    <img src="img/nik-family.jpg" style={{width: '100%', margin: '24px 0px'}}/>
                    <p>In my time away from the office, I treasure spending time trying to be a good father and husband. 
I also enjoy volunteering, landscaping, expressing myself through architecture, art and décor, and trying to practice mindfulness in order to make me a better father, husband, attorney and counselor. My family and I thoroughly enjoy living in this best-kept secret, the Sarasota-Bradenton community.</p>
<p>
I am a proud graduate of the University of Florida, College of Business Administration (B.Sc. 1994), University of Florida, Graduate College of Business Administration (M.B.A., Concentration Human Resources Management, 1997) and the University of Florida, Levin College of Law (J.D., 1997).
   
</p>
            </div>
            <div className="col-md-2">
            </div>
            </div>
        </div>
        </section>
    </Layout>
  )
}


export default AreasOfPractice

