import React from 'react'
import Layout from '../components/Layout'
import Hero from '../components/Hero'

export const AreasOfPractice = () => {

  return (
    <Layout>
        <section className="section section--gradient">
        <div className="container">
            <div className="row">
            <div className="col-md-2">
            </div>
            <div className="col-md-8">
                <div className="section">
                    <Hero hero={'img/about-hero.jpg'} />
                    <h2 className="title is-size-3 has-text-weight-bold is-bold-light">
                        Testimonials
                    </h2>
                    <strong>Adam Rankin - Vice President - Absolute Aluminum, Inc., Venice/Sarasota</strong>
                    <p>
                    “Over the Years the Hultman + Joshi team has provided Absolute Aluminum with sound, clear, and practical legal advice on various issues. They are diligent  at setting companies and individuals up for success. There is nothing better than realizing right when an issue comes up that you are covered thanks to their pre-planning of your possible liabilities. The most welcoming quality is they are a down to earth group that doesn’t just tell you what you want to hear.” 
                    </p>
                    <strong>Debbie Muno, President - Genos North America, Tampa, Florida </strong>
                    <p>“Trust is the foundation of relationships and Hultman + Joshi are our trusted legal advisors, with a personal touch – we feel so much more than ‘a client’ – we feel their care and dedication to our business and to us as people.”</p>
                    <strong>Murray Chase, Executive Director -  Venice Theatre, Venice, Florida </strong>
                    <p>We have used Hultman & Joshi in a variety of ways in the past three years.  They are compassionate, concerned, knowledgeable, and timely. We give them our highest recommendation. </p>
                    <strong>Jonathan S.Y. Cheng - Vice President - Merits Health Products CO., LTD, Taiwan and Cape Coral, Florida</strong>
                    <p>For more than 10 years working with Nik, he is always there for me during the most difficult time or the worst cases. Not only the professionalism but also the care Nik demonstrates to me handling all those hardships is the reason I always think about him and only him whenever needed. Where else can you find such a labor attorney to be on you (the owner’s) side?</p>
                    <strong>Mr. Shashi Galipalli, Forever Veterinarians, Jacksonville, Florida </strong>
                    <p>“I am grateful for the help that Nik provided me during the very difficult times of COVID 19 related employee support. I had some complex employment issues/ concerns that he helped me with the larger picture in mind. He understands how to protect your business and helps you plan day to day employment operations to avoid future complex problems.  His prompt response was extremely helpful and avoided lots of stressful situations.  He was amazing, and he treated me as if I was one of their most important clients. I have never worked with another professional as responsive as Nick.  For that, Nikhil (Nik) gets my respect and my unqualified endorsement.  He is one phone call/text away”   </p>
                    <strong>Angela Ward, Corporate General Counsel, MasterCorp, Nashville, Tennessee</strong>
                    <p>We have worked with the attorneys and support team at Hultman Joshi since 2018 on matters involving the EEOC and the NLRB and have been pleased with not only the outcome of the matters but the relationship developed over the years.  While the firm is outside counsel, they have learned the nuances of our business as if they were members of our organization which enables us to work more efficiently and reach outcomes that meet our business goals.  </p>
                    <strong>Jon and  Danielle Krawczyk, Superior Pools, Port Charlotte, Florida 
</strong>
                    <p>Hultman + Joshi PA is the absolute best! Nik cares about the company in the big picture, not just helping on a particular issue. Anytime you need anything they are there in a timely fashion with great advice, and wonderful communication. We would highly recommend Nik and Hultman+Joshi!!
</p>
                    <strong>Bharat Patel, owner of hotels/businesses, and national AAHOA leader, Sarasota, Florida
</strong>
                    <p>Nikhil and his firm really look out for the hotel community’s best interests when faced with legal risks and challenges. I’ve always found him to be relatable, easy to speak with, and trustworthy. A true counselor. Not just a lawyer.
</p>
                   
                </div>
            </div>
            <div className="col-md-2">
            </div>
            </div>
        </div>
        </section>
    </Layout>
  )
}


export default AreasOfPractice

