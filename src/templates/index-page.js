import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql } from 'gatsby'
import Layout from '../components/Layout'
import AOP from '../components/AOP'
import Hero from '../components/Hero'
import HomeBlogRoll from '../components/HomeBlogRoll'
import Button from '../components/Button'
import TestimonialsHome from '../components/TestimonialsHome'

export const IndexPageTemplate = ({
  image,
  hero,
  aboutSection,
  testimonials,
  areasOfPractice,
}) => (
    <div>
      <Hero hero={hero} />
      <div className="off-white-section">
        <div className="container">
          <div className="row">
            <div className="col-md-2 visible-md">
            </div>
            <div className={"col-xs-12 col-md-8"}>
              <h1 className="section-title">{aboutSection.headline}</h1>
              <p className="center">{aboutSection.description}</p>
              <Button to="/about" value="About Us" className="center-btn" />
            </div>
            <div className="col-md-2 visible-md">
            </div>
          </div>
        </div>
      </div>
      {/* <section>
        <h2 className="section-title" style={{ marginBottom: '8px' }}>{areasOfPractice.headline}</h2>
        <h3 className="center">{areasOfPractice.subheadline}</h3>
        <AOP areas={areasOfPractice.area} />
      </section> */}
      {/* <div className="off-white-section">
        <h2 className="section-title" style={{ marginBottom: '8px' }}>{areasOfPractice.headline}</h2>
        <h3 className="center">{areasOfPractice.subheadline}</h3>
        <HomeBlogRoll />
      </div> */}
      <section>
        <div className="container">
      <div className="row">
          <div className="col-md-3 visible-md">
          </div>
          <div className={"col-md-6"}>
            <div className="row">
            <div className="col-md-6">
              <Link to="/lori-hultman">
              <div style={{
                height: '16px', 
                width: '100%',
                backgroundColor: '#81397F',
                display: 'block',
              }}>

              </div>
              <img src="img/lori.jpg" style={{
                width: '100%'
              }}/>
              <div style={{
                padding: '24px',
                color: '#fff',
                backgroundColor: '#58595B',
                marginTop: '-7px'

              }}>
                <strong style={{fontSize: '24px'}}>Lori Hultman</strong>
                <p>Lori Hultman is a Florida native with a BA and law degree from the University of Florida. She is Florida Bar Board Certified in Labor & Employment Law and has about 37 years experience representing...</p>
              </div>
              <div style={{
                padding: '15px 24px',
                color: '#fff',
                fontSize: '14px',
                backgroundColor: '#81397F'
              }}>
                  Connect with Lori
              </div>
              </Link>
              </div>
              <div className="col-md-6">
                <Link to="/nikhil-joshi">
              <div style={{
                height: '16px', 
                width: '100%',
                backgroundColor: '#8CC63E',
                display: 'block',
              }}>

              </div>
              <img src="img/nik.jpg" style={{
                width: '100%'
              }}/>
              <div style={{
                padding: '24px',
                color: '#fff',
                backgroundColor: '#58595B',
                marginTop: '-7px'

              }}>
                <strong style={{fontSize: '24px'}}>Nikhil Joshi</strong>
                <p>I am Mr. Nikhil N. Joshi, Esq., known also as Nik to my clients and friends, and am Board Certified as a specialist in Labor and Employment Law. I have over 17 years of experience representing employers...</p>
              </div>
              <div style={{
                padding: '15px 24px',
                color: '#000',
                fontSize: '14px',
                backgroundColor: '#8CC63E'
              }}>
                  Connect with Nikhil
              </div>
              </Link>
            </div>
          </div>
          </div>
          <div className="col-md-3 visible-md">
          </div>
        </div>
        
        </div>
      </section>
      <section>
        <h2 className="section-title" style={{ marginBottom: '8px' }}>Happy Clients</h2>
        <h3 className="center">Read our Testimonials</h3>
        <TestimonialsHome testimonialItems={testimonials.testimonial} />
      </section>
    </div>
  )

IndexPageTemplate.propTypes = {
  hero: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  logo: PropTypes.string,
  aboutSection: PropTypes.object,
  intro: PropTypes.shape({
    blurbs: PropTypes.array,
  }),
  backgroundImage: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  areasOfPractice: PropTypes.shape({
    area: PropTypes.array,
  }),
  testimonials: PropTypes.shape({
    testimonial: PropTypes.array,
  })
}

const IndexPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <IndexPageTemplate
        image={frontmatter.image}
        text={frontmatter.text}
        hero={frontmatter.hero}
        aboutSection={frontmatter.aboutSection}
        backgroundImage={frontmatter.backgroundImage}
        logo={frontmatter.logo}
        areasOfPractice={frontmatter.areasOfPractice}
        headline={frontmatter.headline}
        description={frontmatter.description}
        subheadline={frontmatter.subheadline}
        title={frontmatter.title}
        testimonials={frontmatter.testimonials}
        intro={frontmatter.intro}
      />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
        markdownRemark(frontmatter: {templateKey: {eq: "index-page" } }) {
        frontmatter {
          hero {
            childImageSharp {
              fluid(maxWidth: 2048, quality: 100) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          aboutSection {
            headline
            description
          }
          areasOfPractice {
          headline
            subheadline
            area {
              title
              description
            }
          }
          testimonials {
            testimonial {
              quote
              name
            }
          }
        }
      }
    }
`
