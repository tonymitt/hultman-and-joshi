import React from 'react'
import PropTypes from 'prop-types'
import { graphql, Link } from 'gatsby'
import Layout from '../components/Layout'
import Content, { HTMLContent } from '../components/Content'
import Hero from '../components/Hero'

export const AboutPageTemplate = ({ title, content, contentComponent }) => {
  const PageContent = contentComponent || Content

  return (
    <>
    <section className="section section--gradient">
      <div className="container">
        <div className="row">
          <div className="col-md-2">
          </div>
          <div className="col-md-8">
            <div className="section">
              <Hero hero={'img/about-hero.jpg'} />
              <h2 className="title is-size-3 has-text-weight-bold is-bold-light">
                {title}
              </h2>
              <PageContent className="content" content={content} />
            </div>
            
          </div>
          <div className="col-md-2">
          </div>
        </div>
      </div>
    </section>
    <section>
        <div className="container">
      <div className="row">
          <div className="col-md-3 visible-md">
          </div>
          <div className={"col-md-6"}>
            <div className="row">
            <div className="col-md-6">
              <Link to="/lori-hultman">
              <div style={{
                height: '16px', 
                width: '100%',
                backgroundColor: '#81397F',
                display: 'block',
              }}>

              </div>
              <img src="img/lori.jpg" style={{
                width: '100%'
              }}/>
              <div style={{
                padding: '24px',
                color: '#fff',
                backgroundColor: '#58595B',
                marginTop: '-7px'

              }}>
                <strong style={{fontSize: '24px'}}>Lori Hultman</strong>
                <p>Lori Hultman is a Florida native with a BA and law degree from the University of Florida. She is Florida Bar Board Certified in Labor & Employment Law and has about 37 years experience representing...</p>
              </div>
              <div style={{
                padding: '15px 24px',
                color: '#fff',
                fontSize: '14px',
                backgroundColor: '#81397F'
              }}>
                  Connect with Lori
              </div>
              </Link>
              </div>
              <div className="col-md-6">
                <Link to="/nikhil-joshi">
              <div style={{
                height: '16px', 
                width: '100%',
                backgroundColor: '#8CC63E',
                display: 'block',
              }}>

              </div>
              <img src="img/nik.jpg" style={{
                width: '100%'
              }}/>
              <div style={{
                padding: '24px',
                color: '#fff',
                backgroundColor: '#58595B',
                marginTop: '-7px'

              }}>
                <strong style={{fontSize: '24px'}}>Nikhil Joshi</strong>
                <p>I am Mr. Nikhil N. Joshi, Esq., known also as Nik to my clients and friends, and am Board Certified as a specialist in Labor and Employment Law. I have over 17 years of experience representing employers...</p>
              </div>
              <div style={{
                padding: '15px 24px',
                color: '#000',
                fontSize: '14px',
                backgroundColor: '#8CC63E'
              }}>
                  Connect with Nikhil
              </div>
              </Link>
            </div>
          </div>
          </div>
          <div className="col-md-3 visible-md">
          </div>
        </div>
        
        </div>
      </section>
    </>
  )
}


AboutPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
  hero: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
}

const AboutPage = ({ data }) => {
  const { markdownRemark: post } = data

  return (
    <Layout>
      <AboutPageTemplate
        contentComponent={HTMLContent}
        title={post.frontmatter.title}
        content={post.html}
        hero={post.frontmatter.hero}
      />
    </Layout>
  )
}

AboutPage.propTypes = {
  data: PropTypes.object.isRequired,
}

export default AboutPage

export const aboutPageQuery = graphql`
  query AboutPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        hero {
          childImageSharp {
            fluid(maxWidth: 1200, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        title
      }
    }
  }
`
