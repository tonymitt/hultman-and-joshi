import React from 'react'
import PropTypes from 'prop-types'
import ColorBar from '../components/ColorBar'

const Hero = ({ hero }) => (
  <div>
    <div
      className="home-hero"
      style={{
        backgroundImage: `url(${
          !!hero.childImageSharp ? hero.childImageSharp.fluid.src : hero
          })`,
      }}
    >
    </div>
    <ColorBar />
  </div>
)

Hero.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      hero: PropTypes.string,
    })
  ),
}

export default Hero