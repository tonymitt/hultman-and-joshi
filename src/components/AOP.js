import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Button from '../components/Button'
import PreviewCompatibleImage from '../components/PreviewCompatibleImage'

const AOP = ({ areas }) => {

  const [visibleIndex, setVisibleIndex] = useState(0)

  const carouselClick = ({ event }) => {
    setVisibleIndex(event.target.index)
    event.preventDefault()
  }
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-4" style={{ alignSelf: "center" }}>
            <div className="vline"></div>
            <div className="areas-of-practice-list-homepage">
              {areas.map((item, index) => (
                <button onClick={() => setVisibleIndex(index)} onKeyDown={() => setVisibleIndex(index)}
                  className={visibleIndex === index ? 'bold' : 'not-bold'} accessibilityLabel={item.title}
                  ariaControls="areas-of-practice-description-homepage" ariaExpanded={visibleIndex === index ? "true" : "false"}
                >{item.title}</button>
              ))}

            </div>
          </div>
          <div className="col-md-8" style={{ alignSelf: "center" }}>
            <div className="areas-of-practice-description-homepage" id="areas-of-practice-description-homepage">
              {areas.map((item, index) => (
                <div className={visibleIndex === index ? 'showArea' : 'hideArea'}>
                  <h2>{item.title}</h2>
                  <p>{item.description}</p>
                  <Button value="Learn More" style={{ marginTop: '24px' }}></Button>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

AOP.propTypes = {
  areas: PropTypes.arrayOf(
    PropTypes.shape({
      image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
      text: PropTypes.string,
    })
  ),
}

export default AOP
