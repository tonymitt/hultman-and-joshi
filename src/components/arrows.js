import React from 'react'
import PropTypes from 'prop-types'
import Icon from '../components/icons/icons'

const SlideArrows = ({ className, to, onClick, prev }) => (
  <button type="button" onClick={onClick} className={`${className} ${prev ? 'carousel-previous' : 'carousel-next'}`} aria-label={to}>
    {prev ? <Icon.leftArrow color={'#58595B'} /> : <Icon.rightArrow color={'#58595B'} />}
  </button>
)

SlideArrows.propTypes = {
  prev: PropTypes.bool
}

export default SlideArrows