import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'

const Button = ({ icon, to, value, className, style, }) => (
  <>
    <div className={`button-container`} style={style}>
      <Link className={`button ${icon ? 'icon-button' : ''} ${className}`} to={to}>
        {icon ? <img src={value} /> : value}
      </Link>
    </div>
  </>
)

Button.propTypes = {
  value: PropTypes.string,
  to: PropTypes.string,
  icon: PropTypes.bool
}

export default Button