import React from 'react'
import PropTypes from 'prop-types'
import Slider from "react-slick";
import SliderArrow from '../components/arrows'


const TestimonialsHome = ({ testimonialItems }) => {
  var settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: <SliderArrow to="prev" prev />,
    nextArrow: <SliderArrow to="next" />
  };
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-2">
          </div>
          <div className="col-md-8 testimonial-container" style={{ paddingBottom: '48px', position: 'relative' }}>
            <Slider {...settings}>
              {testimonialItems.map(testimonialItem => (
                <div>
                  <p className="testimonial-quote">{testimonialItem.quote}</p>
                  <p className="testimonial-name">{testimonialItem.name}</p>
                </div>
              ))}
            </Slider>
          </div>
          <div className="col-md-2">
          </div>
        </div>

      </div>
    </>
  )
}

TestimonialsHome.propTypes = {
  testimonial: PropTypes.arrayOf(
    PropTypes.shape({
      quote: PropTypes.string,
      name: PropTypes.string,
    })
  ),
}

export default TestimonialsHome
