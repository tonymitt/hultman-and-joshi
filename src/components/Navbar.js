import React, { useState } from 'react'
import { Link } from 'gatsby'
import Icon from '../components/icons/icons'
import HamburgerIcon from '../img/Menu.svg'

const Navbar = () => {
  const [hamburgerActive, setHamburgerActive] = useState(false)
  return (
    <nav
      className="navbar"
      role="navigation"
      aria-label="main-navigation"
      style={{boxShadow: '0px 3px 6px rgba(0,0,0,.16)'}}
    >
      <div className={`mobile-navbar-content ${hamburgerActive ? 'mobile-nav-active' : ''}`}>
        <Link className="navbar-item" to="/">
          Areas of Practice
        </Link>
        <Link className="navbar-item" to="/">
          About
        </Link>
        <Link className="navbar-item" to="/news">
          News
        </Link>
        <Link className="navbar-item" to="/">
          Contact
        </Link>
      </div>
      <div className="navbar-brand">
        <div
          className={`navbar-burger burger ${hamburgerActive ? 'true' : 'false'}`}
          data-target="navMenu"
          onClick={() => hamburgerActive ? setHamburgerActive(false) : setHamburgerActive(true)}
        >
          <img src={HamburgerIcon} alt="LinkedIn Social Link" style={{ width: '24px' }} />
        </div>
        <Link to="/" className="navbar-item" title="Hultman + Joshi, P.A. Labor and Employment Counselors and Litigators" style={{ lineHeight: '0px' }}>
          <Icon.whiteLogo size="38" />
        </Link>
        {/* Hamburger menu */}

      </div>
      <div
        id="navMenu"
        className={`navbar-menu`}
      >
        <div className="navbar-content">
          <Link className="navbar-item" to="/about">
              About
          </Link>
          <Link className="navbar-item" to="/areas-of-practice">
            Areas of Practice
          </Link>
          
          <Link className="navbar-item" to="/testimonials">
            Testimonials
              </Link>
          <Link className="navbar-item" to="/contact">
            Contact
              </Link>
        </div>
      </div>
      <div className="navbar-contact">
        <span>
          <span className="phoneNumber" style={{color: '#000'}}>
          941.218.2800
            </span>
          {/* <Link className="navbar-icon" to="/" title="LinkedIn Social Link">
            <Icon.linkedin />
          </Link>
          <Link className="navbar-icon" to="/" title="Facebook Social Link">
            <Icon.facebook />
          </Link> */}
        </span>
      </div>
    </nav>
  )
}

export default Navbar
