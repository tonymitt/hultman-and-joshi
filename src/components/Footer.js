import React from 'react'
import { Link } from 'gatsby'
import Button from '../components/Button'
import Icon from '../components/icons/icons'
import Fax from '../img/Fax.svg'
import Phone from '../img/Phone.svg'
import Mail from '../img/Mail.svg'
import ColorBar from '../components/ColorBar'

const Footer = class extends React.Component {
  render() {
    return (
      <footer className="footer has-background-black has-text-white-ter">
        <div className="top-footer-bar">
         
          <Link to="/about">
            About
          </Link>
           <Link to="/areas-of-practice">
            Areas of Practice
          </Link>
          <Link to="/testimonials">
            Testimonials
          </Link>
          <Link to="/contact">
            Contact
          </Link>
        </div>
        <div className="main-footer">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <h4>About Us</h4>
                <p>Hultman + Joshi guides their clients through the maze of employment laws and regulations that impact employers and employees on a daily basis. Together, we are here to help.</p>
                <Button value="Read More" to="/" />
              </div>
              <div className="col-md-2">
              </div>
              <div className="col-md-3">
                <h4>Address</h4>
                <p>Hultman + Joshi <br />
                  2055 Wood St. <br />
                  Suite 208 <br />
                  Sarasota, FL 34237
                </p>
              </div>
              <div className="col-md-3 footer-contact-info">
                <h4>Contact</h4>
                <p>
                  <a href="mailto:info@hultmanjoshi.com" className="contact-element"><img src={Mail} /><span>info@hultmanjoshi.com</span></a>
                  <span className="contact-element"><img src={Phone} /><span>941.218.2800</span></span>
                  <span className="contact-element"><img src={Fax} /><span>941.218.2801</span></span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="copyright-bar">
          <div className="container">
            <div className="row">
              <div className="col-md-12" style={{textAlign: 'center'}}>
                Copyright &copy; 2021 Hultman & Joshi, P.A. All Rights Reserved
                <Link className="navbar-icon" to="/" title="LinkedIn Social Link" style={{ float: 'right' }}>
                  <Icon.linkedin />
                </Link>
                <Link className="navbar-icon" to="/" title="Facebook Social Link" style={{ float: 'right' }}>
                  <Icon.facebook />
                </Link>
              </div>
            </div>
          </div>
        </div>
        <ColorBar />
      </footer>
    )
  }
}

export default Footer
