import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql, StaticQuery } from 'gatsby'
import PreviewCompatibleImage from './PreviewCompatibleImage'
import Button from '../components/Button'
import RightArrow from '../img/RightArrow.svg'
class HomeBlogRoll extends React.Component {
  render() {
    const { data } = this.props
    const { edges: posts } = data.allMarkdownRemark

    return (
      <div className="container">
        <div className="row">
          {posts &&
            posts.map(({ node: post }) => (
              <div className="col-md-3">
                <Link
                  className="blog-roll-container"
                  to={post.fields.slug}
                >
                  <div className="is-parent column is-6" key={post.id}>
                    <article
                      className={`blog-list-item tile is-child box notification ${
                        post.frontmatter.featuredpost ? 'is-featured' : ''
                        }`}
                    >
                      <header>
                        {post.frontmatter.featuredimage ? (
                          <div className="featured-thumbnail">
                            <PreviewCompatibleImage
                              imageInfo={{
                                image: post.frontmatter.featuredimage,
                                alt: `featured image thumbnail for post ${post.frontmatter.title}`,
                              }}
                              imageStyle={{ borderRadius: '0px' }}
                            />
                          </div>
                        ) : null}
                        <p className="post-title">

                          {post.frontmatter.title}

                        </p>
                      </header>
                      <p className="post-excerpt">
                        {post.excerpt}
                      </p>
                      <div className="date-read-container">
                        <div className="post-date">
                          {post.frontmatter.date}
                        </div>
                        <Button icon to={post.fields.slug} value={RightArrow} />
                      </div>
                    </article>
                  </div>
                </Link>
              </div>
            ))}
        </div>
      </div >
    )
  }
}

HomeBlogRoll.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.array,
    }),
  }),
}

export default () => (
  <StaticQuery
    query={graphql`
      query HomeBlogRollQuery {
        allMarkdownRemark(
          sort: { order: DESC, fields: [frontmatter___date] }
          filter: { frontmatter: { templateKey: { eq: "blog-post" } } }
          limit: 4
        ) {
          edges {
            node {
              excerpt(pruneLength: 110)
              id
              fields {
                slug
              }
              frontmatter {
                title
                templateKey
                date(formatString: "MMMM DD, YYYY")
                featuredpost
                featuredimage {
                  childImageSharp {
                    fluid {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={(data, count) => <HomeBlogRoll data={data} count={count} />}
  />
)
