import React from 'react';

const leftArrow = ({ color = "#fff", size = "24" }) => (
  <svg labeledby="previous-icon-title previous-icon-description" width={size} height={size} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M9.96094 18.9141L11.0391 17.8359L5.95313 12.75L21 12.75L21 11.25L5.95313 11.25L11.0391 6.16406L9.96094 5.08594L3.58594 11.4609L3.07031 12L3.58594 12.5391L9.96094 18.9141Z" fill={color} />
    <title id="previous-icon-title">Previous Testimonial</title>
    <desc id="previous-icon-description">Link to previous testimonial</desc>
  </svg>
)

export default leftArrow