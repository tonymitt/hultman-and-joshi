import whiteLogo from './whiteLogo'
import facebook from './facebook'
import linkedin from './linkedin'
import rightArrow from './rightArrow'
import leftArrow from './leftArrow'

export {
  whiteLogo,
  facebook,
  linkedin,
  rightArrow,
  leftArrow
}

export default {
  whiteLogo,
  facebook,
  linkedin,
  rightArrow,
  leftArrow
}